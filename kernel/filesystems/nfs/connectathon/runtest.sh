#!/bin/sh

# Copyright (c) 2006 Red Hat, Inc. All rights reserved. This copyrighted material 
# is made available to anyone wishing to use, modify, copy, or
# redistribute it subject to the terms and conditions of the GNU General
# Public License v.2.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# Author: Bill Peck <bpeck@redhat.com>

# Environment Variables
# $JOBID
# $DISTRO
# $ARCH
# $TEST
# $FAMILY
# $VARIANT
# $ARGS

# source the test script helpers
. /usr/bin/rhts-environment.sh

function checkRebootCount()
{
    if [ "$REBOOTCOUNT" -gt "0" ] ; then 
	echo "***** System rebooted *****" | tee -a $OUTPUTFILE
	report_result system_rebooted WARN
	exit 0
    fi
}

function checkServers()
{
    foundserver=0
    for S in $servers; do
	server=$(echo $S| cut -f1 -d:)
	ping -c 1 $server >/dev/null 2>&1
	if [ $? != 0 ]; then
	    echo "Warn ***** Server $server not available or unavailable in the $thisdom *****" | tee -a $OUTPUTFILE
	else
	    echo "Pass ***** Server $server available *****" | tee -a $OUTPUTFILE
            foundserver=$(expr "$foundserver" + 1)
	fi
    done
    
    if [ "$foundserver" -gt "0" ]; then
	echo "***** Some servers are available... continue testing *****" | tee -a $OUTPUTFILE
    else
	s390chk=$(/bin/hostname | awk -F. '{print $2}')
	if [ $s390chk = "z900" ]; then
	    report_result $TEST PASS
	else
	    report_result $TEST WARN
	fi
	exit 0
    fi
}

function _cthon04() {
    # redirect stdout & stderr to $OUTPUTFILE
    exec 5>&1 6>&2
    exec >> $OUTPUTFILE 2>&1
    if [ -n "PROFILE" ]; then
        PS4='+ $(date "+%H:%M:%S.%N")\011 '
        set -x
    fi
    # _cthon04 $fstype $test_name $report_path $server $nfspath $options
    fstype=$1
    test_name=$2
    report_path=$3
    server=$4
    nfspath=$5
    options=$6
    test=$(echo $test_name| cut -f1 -d:)
    name=$(echo $test_name| cut -f2 -d:)

    tethereal_out="/mnt/testarea/${server}_${report_path}_${name}"

    NET_DUMPER="tcpdump"
    if [ -f /usr/sbin/tshark ]; then
        NET_DUMPER="/usr/sbin/tshark"
    fi
    if [ -f /usr/sbin/tethereal ]; then
        NET_DUMPER="/usr/sbin/tethereal"
    fi
    if [ -n "$DISABLE_NETDUMPER" ]; then
        NET_DUMPER="/bin/false"
    fi
    echo "NET_DUMPER is $NET_DUMPER"

    $NET_DUMPER -q -i any -w $tethereal_out.cap host $server 2>&1 &
    tpid=$!
    sleep 5

    echo "===== Starting '$report_path' test '$name' ====="
    echo "----- Server load `/usr/bin/rup $server` -----"
    echo "----- start: `/bin/date` -----"
    # log the command we used.
    echo ./server $test -N $testRuns -F $fstype ${options} -p $nfspath $server
    ./server $test -N $testRuns -F $fstype ${options} -p $nfspath $server 2>&1
    result=$?

    echo "----- end: `/bin/date` -----"
    echo "----- Server load `/usr/bin/rup $server` -----"
    echo "----- return code: $result -----"

    kill $tpid 2>&1 &
    
    umount /mnt/$server
    mount
    if mount|grep -q /mnt/$server 2>&1; then
	echo "----- umount failed, pls check umount function -----"
	result=1 
    fi

    # restore stdout & stderr
    if [ -n "PROFILE" ]; then
        set +x
    fi
    exec 1>&5 2>&6

    if [ $result = 0 ]; then
        if [ -z "$SKIP_SUBRESULT" ]; then
            iswarn=`/bin/grep WARNING! $OUTPUTFILE | wc -l`
            if [ $iswarn -gt $result ]; then
                report_result $TEST/$server/$report_path/$name PASS $iswarn
            else
                report_result $TEST/$server/$report_path/$name PASS $result
            fi
        fi
        if [ -z "$SAVECAPTURE" ]; then
            rm -f $tethereal_out.cap > /dev/null 2>&1
        else
            bzip2 $tethereal_out.cap
        fi
    else
        bzip2 $tethereal_out.cap
        rhts_submit_log -l $tethereal_out.cap.bz2
        report_result $TEST/$server/$report_path/$name FAIL $result
        if [ -z "$SAVECAPTURE_FAILED" ]; then
            rm -f $tethereal_out.cap.bz2 > /dev/null 2>&1
        fi
    fi


    # do a lazy unmount so the next test will run
    umount -l /mnt/$server >/dev/null 2>&1
    
    # backup and clear log
    old_log="`mktemp /tmp/tmp.XXXXXX`"
    cp $OUTPUTFILE $old_log
    : > $OUTPUTFILE
    echo "log moved to: '$old_log'"

    return $result
}

# -----------------------
if [ "$CTHONTESTRUNS" ]; then
    # We can specify the number of times to run the Cthon test.
    echo " ============ Override # of test runs ============"
    testRuns="$CTHONTESTRUNS"
else
    # Default # of test runs is 1.
    echo " ========== Use default single test run ============"
    testRuns="1"
fi

if [ "$CTHONSERVERS" ]; then
    # We can specify lists of serves tests againts. 
    # For example, we can provide a customized list of
    # servers CTHONSERVER="host1-nfs host2-nfs host3-nfs"
    echo " ============ Override server list ============"
    servers="$CTHONSERVERS"
else
    # Default lists of servers.
    echo " ========== Use default server list ============"
    servers="rhel5-nfs:/export/home rhel4-nfs:/export/home rhel3-nfs:/export/home sol10-nfs:/export/home sol9-nfs:/export/home netapp-nfs:/vol/vol0/home/cthon rhel6-nfs:/export/home"
fi

if [ "$CTHONTESTS" ]; then
    echo " ============ Override cthon test list ============"
    tests="$CTHONTESTS"
else
    echo " ========== Use default cthon test list ============"
    tests="-b:base -g:general -s:special -l:lock"
fi

if [ "$CTHONTCPTYPE" ]; then
    echo " ============ Override TCP type list ============"
    types="$CTHONTCPTYPE"
else
    echo " ========== Use default TCP type list ============"
    types="udp tcp"
fi

thisdom=`dnsdomainname`
kernversion=$(rpm -q --queryformat '%{version}\n' -qf /boot/config-$(uname -r))

checkRebootCount

checkServers

pushd cthon04
for server_path in $servers; do
    : > result.txt
    SCORE=0

    # start time
    STIME=`date +%s`

    server=$(echo $server_path| cut -f1 -d:)
    nfspath=$(echo $server_path| cut -f2 -d:)

    # Check to see if the CNAME is setup in this lab.
    ping -c 1 $server >/dev/null 2>&1
    if [ $? -ne 0 ]; then continue; fi
    
    # NFSv2 & NFSv3 tests
    nfsvers="nfsvers=2 nfsvers=3"
    for nfsver in $nfsvers; do
        for type in $types; do
 	    for test_name in $tests; do
                # _cthon04 $fstype $test_name $report_path $server $nfspath $options
                _cthon04 nfs $test_name "${nfsver}_${type}" $server $nfspath -o$nfsver,$type
                rc=$?
                [ $rc -ne 0 ] && SCORE=$(expr $SCORE + 1)
                echo -e "$nfsver\t$type\t$test_name\t$rc" >> result.txt
            done
        done
    done

    # NFSv4 tests
    if [ $kernversion != "2.4.21" ]; then
	testnfsvers=$(rpcinfo -t $server nfs | wc -l)
	if [ $testnfsvers -eq 3 ]; then
	    for test_name in $tests; do
                # _cthon04 $fstype $test_name $report_path $server $nfspath $options
		_cthon04 nfs4 $test_name "nfsvers=4" $server $nfspath -oproto=tcp
                rc=$?
                [ $rc -ne 0 ] && SCORE=$(expr $SCORE + 1)
                echo -e "nfsvers=4\ttcp\t$test_name\t$rc" >> result.txt
	    done
	fi
    fi

    # end & test time
    ETIME=`date +%s`
    TTIME=`expr $ETIME - $STIME`
    
    # format result.txt for tabbed alignment
    sed -i 's/lock/lock\t/g;s/base/base\t/g' result.txt
    echo "***** Summary for server '$server': '$SCORE' tests failed *****" | tee -a $OUTPUTFILE
    echo -e "NFS version\tType\tTest\tReturn code" | tee -a $OUTPUTFILE
    cat result.txt | tee -a $OUTPUTFILE
    echo "Total time: $TTIME" | tee -a $OUTPUTFILE

    if [ $SCORE -eq 0 ]; then
	if [ $TTIME -lt 1000 ]; then
	    report_result $TEST/$server PASS $TTIME
	else
	    echo "Time exceeded 1000 seconds" | tee -a $OUTPUTFILE
	    report_result $TEST/$server WARN $TTIME
	fi
    else
        report_result $TEST/$server FAIL $TTIME
    fi
done
popd


