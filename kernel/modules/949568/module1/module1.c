#include <linux/module.h>
#include <linux/kernel.h>

MODULE_LICENSE("Dual BSD/GPL");

static int module1_init(void)
{
	int code = 0;
	printk(KERN_INFO "module1 loaded!\n");
	code = request_module("module2");
	if (code != 0)
		printk(KERN_ERR "Request module2 failed\n");
	else
		printk(KERN_INFO "Request module2 successed\n");
	return 0;
}
static void module1_exit(void)
{
        printk(KERN_ALERT "module1 removed\n");
}
module_init(module1_init);
module_exit(module1_exit);
