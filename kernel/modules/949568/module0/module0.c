#include <linux/module.h>
#include <linux/kernel.h>

MODULE_LICENSE("Dual BSD/GPL");

static int module0_init(void)
{
	int code = 0;
	printk(KERN_INFO "Module0 loaded!\n");
	code = request_module("module1");
	if (code != 0)
		printk(KERN_ERR "Request module1 failed\n");
	else
		printk(KERN_INFO "Request module1 successed\n");
	return 0;
}
static void module0_exit(void)
{
        printk(KERN_ALERT "module0 removed\n");
}
module_init(module0_init);
module_exit(module0_exit);
