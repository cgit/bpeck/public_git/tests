#include <linux/module.h>
#include <linux/kernel.h>

MODULE_LICENSE("Dual BSD/GPL");

static int module2_init(void)
{
	printk(KERN_INFO "module2 loaded!\n");
	return 0;
}
static void module2_exit(void)
{
        printk(KERN_ALERT "module2 removed\n");
}
module_init(module2_init);
module_exit(module2_exit);
