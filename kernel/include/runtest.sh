#!/bin/sh 
#
### Kernel Testing Include File

### Include these variables
#
# control where to log debug messages to:
# devnull = 1 : log to /dev/null
# devnull = 0 : log to file specified in ${DEBUGLOG}
devnull=0

# Create debug log
DEBUGLOG=`mktemp -p /mnt/testarea -t DeBug.XXXXXX`

# locking to avoid races
lck=$OUTPUTDIR/$(basename $0).lck

# Kernel Variables
K_NAME=`rpm -q --queryformat '%{name}\n' -qf /boot/config-$(uname -r)`
# example output: kernel
K_VER=`rpm -q --queryformat '%{version}\n' -qf /boot/config-$(uname -r)`
# example output: 2.6.32
K_REL=`rpm -q --queryformat '%{release}\n' -qf /boot/config-$(uname -r)`
# example output: 220.el6
K_SRC=`rpm -q --queryformat 'kernel-%{version}-%{release}.src.rpm\n' -qf /boot/config-$(uname -r)`
# example output: kernel-2.6.32-220.el6.src.rpm
K_BASE=`rpm -q --queryformat '%{name}-%{version}-%{release}.%{arch}\n' -qf /boot/config-$(uname -r)`
# example output: kernel-2.6.32-220.el6.x86_64
K_ARCH=$(rpm -q --queryformat '%{arch}' -f /boot/config-$(uname -r))
# example output: x86_64
# example output: armv7hl
# example output: armv7l
K_RUNNING=$(uname -r)
# example output: 2.6.32-220.el6.x86_64
K_RUNNING_VR=$(uname -r | sed -e "s/\.${K_ARCH}//")
# example output: 3.6.10-8.fc18.highbank
#
RH_REL=`cat /etc/redhat-release | cut -d" " -f7`
# example output: 6.2
TESTAREA="/mnt/testarea"

### Include these functions
#
# Print out a header
function DefaultHeader ()
{
    echo "*********************************************"
    echo "**        Sourcing: /kernel/include        **"
    echo "*********************************************"

}

# Log a message to the ${DEBUGLOG} or to /dev/null
function DeBug ()
{
    local msg="$1"
    local timestamp=$(date '+%F %T')
    if [ "$devnull" = "0" ]; then
        (
            flock -x 200 2>/dev/null
            echo -n "${timestamp}: " >>$DEBUGLOG 2>&1
            echo "${msg}" >>$DEBUGLOG 2>&1
        ) 200>$lck
    else
        echo "${msg}" >/dev/null 2>&1
    fi
}

function RprtRslt ()
{
    echo "" | tee -a $OUTPUTFILE
    echo "***** End of runtest.sh *****" | tee -a $OUTPUTFILE

    TEST=$1

    # Default result to FAIL
    export RESULT="FAIL"

    # SCORE of 0 is PASS. SCORE of 99 is PASS, as test is skipped
    # If no SCORE is given, default to fail and count the reported fails
    # Then post-process the results to find the regressions
    if test -z "$2" ; then
      export SCORE=`cat $OUTPUTFILE | grep "FAILED: " | wc -l`
    else
      export SCORE=$2
    fi

    if test ! -s "$OUTPUTFILE" ; then
        export RESULT="FAIL"
    else
        if [ "$SCORE" -eq "0" ] || [ "$SCORE" -eq "99" ]; then
            export RESULT="PASS"
        else
            export RESULT="FAIL"
        fi
    fi

    # File the results in the database
    report_result $TEST $RESULT $SCORE
    SubmitLog $DEBUGLOG
    exit 0
}

function SubmitLog ()
{
    LOG=$1
    rhts_submit_log -S $RESULT_SERVER -T $TESTID -l $LOG
}

# EndFile
