#!/bin/sh

# Include Beaker environment
. /usr/bin/rhts-environment.sh || exit 1

echo -n "uname -s: "
uname -s
echo -n "uname -n: "
uname -n
echo -n "uname -r: "
uname -r
echo -n "uname -v: "
uname -v
echo -n "uname -m: "
uname -m
echo -n "uname -p: "
uname -p
echo -n "uname -i: "
uname -i
echo -n "uname -o: "
uname -o

echo -n "kernel rpm arch: "
rpm -qf --qf '%{arch}\n' $(ls /boot/vmlinuz* | head -n 1)

report_result $TEST PASS 0
