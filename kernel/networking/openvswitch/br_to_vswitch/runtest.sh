#!/bin/bash
# vim: dict=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /kernel/networking/openvswitch/br_to_vswitch
#   Description: switch all bridge interfaces over to openvswitch
#   Author: Bill Peck <bpeck@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2012 Red Hat, Inc. All rights reserved.
#
#   This copyrighted material is made available to anyone wishing
#   to use, modify, copy, or redistribute it subject to the terms
#   and conditions of the GNU General Public License version 2.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE. See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public
#   License along with this program; if not, write to the Free
#   Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#   Boston, MA 02110-1301, USA.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/bin/rhts-environment.sh || exit 1

PACKAGE="openvswitch"
BRIDGES=${BRIDGES:="$(brctl show | awk 'NR>1 && /^[^\t]/ {printf $1" "}')"}
count=0

convert_bridge()
{
bridge=$1
vswitch=$2

cat <<EOF >/etc/sysconfig/network-scripts/ifcfg-$vswitch
DEVICE=$vswitch
ONBOOT=yes
DEVICETYPE=ovs
TYPE=OVSBridge
HOTPLUG=no
EOF

bridgemac=$(cat /sys/class/net/$bridge/address)
for intf in $(ls /sys/class/net); do
    if [ "$intf" = "$bridge" ]; then 
        continue
    fi
    devicemac=$(cat /sys/class/net/$intf/address)
    if [ "$devicemac" = "$bridgemac" ]; then
        device=$intf
        break
    fi
done
if [ -n "$device" ]; then
cat <<EOF >>/etc/sysconfig/network-scripts/ifcfg-$vswitch
OVSBOOTPROTO="dhcp"
OVSDHCPINTERFACES="$device"
EOF
fi

# Remove the old bridge
if [ -e "/etc/sysconfig/network-scripts/ifcfg-$bridge" ]; then
    rm -f "/etc/sysconfig/network-scripts/ifcfg-$bridge"
fi
}

convert_libvirt()
{
# Convert libvirt guests using $bridge as well.
bridge=$1
vswitch=$2

for virt in $(ls /etc/libvirt/qemu/*.xml); do
    echo Converting $virt from bridge to openvswitch
    grep -w $vswitch $virt
    if [ $? -ne 0 ];then 
        sed  -ie "s/$bridge/$vswitch/; /$vswitch/ a \<virtualport type=\"openvswitch\"/>" $virt
        virsh define --file $virt
    fi
done
}

convert_port()
{
device=$1
vswitch=$2

# Don't attempt to convert ports which are dyamic
if [ -e "/etc/sysconfig/network-scripts/ifcfg-$device" ]; then
cat <<EOF >/etc/sysconfig/network-scripts/ifcfg-$device
DEVICE=$device
ONBOOT=yes
DEVICETYPE=ovs
TYPE=OVSPort
OVS_BRIDGE=$vswitch
BOOTPROTO=none
HOTPLUG=no
EOF
fi
}

# Install $PACKAGE if not installed and activate.
rpm -q $PACKAGE || fail "$PACKAGE not installed"
chkconfig --add $PACKAGE
service $PACKAGE start

for bridge in $BRIDGES; do
    vswitch="ovsbridge$count"
    count=$(($count + 1))

    # Convert /etc/sysconfig/network-scripts/ifcfg-br*
    convert_bridge $bridge $vswitch
    convert_libvirt $bridge $vswitch
    # Remove interfaces from br0 and add them to vlan-br
    while [ "$(brctl show $bridge | awk "/$bridge/ {print \$4}")" != "" ]; do
        intf=$(brctl show $bridge | awk "/$bridge/ {print \$4}")
        brctl delif $bridge $intf
        convert_port $intf $vswitch && report_result $TEST/$intf PASS 0 || report_result $TEST/$intf FAIL $?
    done
done

# Stop all virt guests
cmd=""
for virt in $(virsh list | awk 'NR>2 {print $2}'); do
    virsh destroy $virt
    # build delayed start command
    cmd="virsh start $virt; $cmd"
done

# Restart network using openvswitch
service network restart

# Restart virt guests
sh -c "$cmd"
ovs-vsctl show
