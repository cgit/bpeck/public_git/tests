#!/bin/bash -x
# vim: dict=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /kernel/networking/openvswitch/libvirt/add_interface
#   Description: add $SWITCHES interface to guests
#   Author: Bill Peck <bpeck@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2012 Red Hat, Inc. All rights reserved.
#
#   This copyrighted material is made available to anyone wishing
#   to use, modify, copy, or redistribute it subject to the terms
#   and conditions of the GNU General Public License version 2.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE. See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public
#   License along with this program; if not, write to the Free
#   Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#   Boston, MA 02110-1301, USA.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/bin/rhts-environment.sh || exit 1

PACKAGE="openvswitch"
rpm -q $PACKAGE || fail "$PACKAGE not installed"

find_free_slot()
{
    local guest=$1
    slot=$(cat $guest | awk '/<address/ { print }' | sed -e "s/.*slot='0x\([^']*\)'.*/\1/" | sort | tail -n 1 | tr '[:lower:]' '[:upper:]')
    printf "0x%02x\n" $(($(echo "ibase=16; $slot" | bc) +1 ))
}

add_interface()
{
    local guest=$1
    local switch=$2
    local slot=$(find_free_slot $guest)
    sed -ie "/<\/devices>/ i\
<interface type='bridge'>\n\<source bridge='$switch'\/>\n\<virtualport type='openvswitch'\/>\n\<model type='virtio'\/>\n\<address type='pci' domain='0x0000' bus='0x00' slot='$slot' function='0x0'\/>\n\<\/interface>
" $guest
   return $?
}

add_tag()
{
    local guest=$1
    local switch=$2
    local tag=$3
    sed -ie "/<source bridge='$switch'\/>/ a \<vlan>\n<tag id='$tag'\/>\n<\/vlan>" $guest
    return $?
}

for guest in $GUESTS; do
    for config in $(eval echo \$$guest); do
        switch=$(echo $config| sed -n 's/.*switch=\([^\;]*\).*/\1/p')
        tag=$(echo $config| sed -n 's/.*tag=\([^\;]*\).*/\1/p')
        add_interface /etc/libvirt/qemu/${guest}.xml $switch
        if [ -n "$tag" ];then
            add_tag /etc/libvirt/qemu/${guest}.xml $switch $tag
        fi
    done
    virsh define --file /etc/libvirt/qemu/${guest}.xml && report_result $TEST/$guest/$config PASS 0 || report_result $TEST/$guest/$config FAIL 1
done
