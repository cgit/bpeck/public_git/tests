#!/bin/bash -x
# vim: dict=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /kernel/networking/libvirt/ping
#   Description: pings both $VALID_ADDRS and $INVALID_ADDRS, verifies connectivity
#   Author: Bill Peck <bpeck@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2012 Red Hat, Inc. All rights reserved.
#
#   This copyrighted material is made available to anyone wishing
#   to use, modify, copy, or redistribute it subject to the terms
#   and conditions of the GNU General Public License version 2.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE. See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public
#   License along with this program; if not, write to the Free
#   Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#   Boston, MA 02110-1301, USA.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/bin/rhts-environment.sh || exit 1

PING_COUNT=${PING_COUNT:="10"}
PING_SIZE=${PING_SIZE:="64"}
fail=0

pass()
{
    report_result $TEST/$WHITEBOARD/$guest/$1 PASS 0
}

fail()
{
    report_result $TEST/$WHITEBOARD/$guest/$1/$addr FAIL 1
    fail=1
}

for guest in $GUESTS; do
    for blob in $(eval echo \$$guest); do
        eval $(echo $blob| sed 's/;/\\;/g')
    done
    pass Running
    IFS=";"
    for addr in $valid_addrs; do
        ../common/autologin.exp $guest "ping -c $PING_COUNT -i 0.2 -s $PING_SIZE $addr" || fail VALID
    done
    for addr in $invalid_addrs; do
        ../common/autologin.exp $guest "ping -c $PING_COUNT -i 0.2 -s $PING_SIZE $addr" && fail INVALID
    done
    unset IFS
done
